"# vim: tabstop=2 shiftwidth=2 expandtab

"dein Scripts-----------------------------
if &compatible
  set nocompatible               " Be iMproved
endif

" Required:
set runtimepath+=~/.cache/dein/repos/github.com/Shougo/dein.vim

" Required:
if dein#load_state('~/.cache/dein')
  call dein#begin('~/.cache/dein')

  " Let dein manage dein
  " Required:
  call dein#add('~/.cache/dein/repos/github.com/Shougo/dein.vim')

  " Add or remove your plugins here:
  call dein#add('neomake/neomake')
  call dein#add('Shougo/deoplete.nvim')
  call dein#add('Shougo/neosnippet.vim')
  call dein#add('Shougo/neosnippet-snippets')
  call dein#add('autozimu/LanguageClient-neovim')
  call dein#add('rust-lang/rust.vim')
  call dein#add('tomasr/molokai')

  " Required:
  call dein#end()
  call dein#save_state()
endif

" Required:
filetype plugin indent on
syntax enable

" If you want to install not installed plugins on startup.
"if dein#check_install()
"  call dein#install()
"endif

"End dein Scripts-------------------------

"Rust filetype----------------------------
autocmd BufReadPost *.rs setlocal filetype=rust

" General---------------------------------
set completeopt-=preview

set expandtab
set shiftwidth=2
set softtabstop=2

set number
"End general------------------------------

"Deoplete settings------------------------
let g:deoplete#enable_at_startup = 1
"End deoplete settings--------------------

"color theme------------------------------
set termguicolors
let g:rehash256 = 1
colorscheme molokai 
let g:molokai_original = 1
"End color theme--------------------------

"Language server support------------------
" Required for operations modifying multiple buffers like rename.
set hidden

let g:LanguageClient_serverCommands = {
    \ 'rust': ['rustup', 'run', 'nightly', 'rls'],
    \ }

" Automatically start language servers.
let g:LanguageClient_autoStart = 1

nnoremap <silent> K :call LanguageClient_textDocument_hover()<CR>
nnoremap <silent> gd :call LanguageClient_textDocument_definition()<CR>
nnoremap <silent> <F2> :call LanguageClient_textDocument_rename()<CR>


